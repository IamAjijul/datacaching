package com.test.datacaching.network;


public interface Urls {

    String TAG_VOLLEY_REQUEST_QUEUE = "volley_request_queue";

    String BASE_URL = "https://reqres.in/api/users?per_page=7";// DEV

    String URL_GET_USERS = BASE_URL + "&page=";


}
