package com.test.datacaching.network;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.test.datacaching.R;
import com.test.datacaching.application.DataCaching;
import com.test.datacaching.helper.AppUtilities;
import com.test.datacaching.helper.Logger;
import com.test.datacaching.helper.SquareLoadingAnimation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class RequestClient {

    Context context;
    //private ProgressDialog dialog;
    private AlertDialog.Builder builder;
    private String TAG = "RequestClient";
    private LayoutInflater vi;
    private View progressLayout;
    private LayoutInflater mLayoutInflater;


    // Tag used to cancel the request
    public RequestClient(Context mContext) {
        this.context = mContext;
        //dialog = new ProgressDialog(context);
        //dialog.getWindow().setContentView(R.layout.progress_loader_view);
        builder = new AlertDialog.Builder(context);
        mLayoutInflater = LayoutInflater.from(context);

        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void showLoaderView(View parentView, boolean showLoadingtext, boolean isFullScreen) {
        progressLayout = LayoutInflater.from(context).inflate(R.layout.progress_loader_view, null);
        TextView loading_text = (TextView) progressLayout.findViewById(R.id.loading_text);
        SquareLoadingAnimation progressLoader = (SquareLoadingAnimation) progressLayout.findViewById(R.id.progress_loader);
        if (!showLoadingtext)
            loading_text.setVisibility(View.GONE);
        RelativeLayout.LayoutParams position = null;
        if (isFullScreen) {
            position = new RelativeLayout.LayoutParams
                    (RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT);
        } else {
            position = new RelativeLayout.LayoutParams
                    (RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
        }
        position.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressLayout.setLayoutParams(position);
        if (parentView instanceof RelativeLayout) {
            RelativeLayout v = (RelativeLayout) parentView;
            v.addView(progressLayout);
        }
    }

    private void dismissLoaderView() {
        if (progressLayout != null)
            progressLayout.setVisibility(View.GONE);
        progressLayout = null;
    }

    //Get Users
    public void callGetUsersApi(boolean showFullScreenLoader, Context context, int page, View containerView,
                                boolean showLoaderText, BaseResponseListener mBaseResponseListener) {
        showLoaderView(containerView, showLoaderText, showFullScreenLoader);
        postLoader(context, Urls.URL_GET_USERS + page, mBaseResponseListener);
    }

    private void postLoader(final Context context, String url, final BaseResponseListener mBaseResponseListener) {
        Logger.showDebugLog("URL: " + url);

        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    Logger.setLogTag(TAG);
                    Logger.showDebugLog("Response: " + response);
                    mBaseResponseListener.requestCompleted(jsonString);
                    dismissLoaderView();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mBaseResponseListener.requestEndedWithError(context.getResources().getResourceEntryName(R.string.something_went_wrong));
                dismissLoaderView();
            }
        });
        /*StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.setLogTag(TAG);
                Logger.showDebugLog("Response: " + response);
                mBaseResponseListener.requestCompleted(response);
                dismissLoaderView();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mBaseResponseListener.requestEndedWithError(error);
                dismissLoaderView();
            }
        }) ;*/
        /**
         * Timeout 10 seconds
         */
        //  int socketTimeout = 90 * 1000;//10 seconds - can be changed as required

//        sr.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setTag(Urls.TAG_VOLLEY_REQUEST_QUEUE); //Set Tag
        RequestQueue queue = DataCaching.getInstance().getRequestQueue();
        Logger.showDebugLog("++++++++++++++++++++++++++++++++++++++++++++ " + queue.getCache().get(url));

        if (queue.getCache().get(url) != null && !AppUtilities.isOnline(context)) {
            //response exists
            String cachedResponse = new String(queue.getCache().get(url).data);
            Logger.setLogTag(TAG);
            Logger.showDebugLog("Response: " + cachedResponse);
            mBaseResponseListener.requestCompleted(cachedResponse);
            dismissLoaderView();
        } else if (!AppUtilities.isOnline(context)) {
            mBaseResponseListener.requestEndedWithError(context.getResources().getString(R.string.noInternet));
            dismissLoaderView();
        } else {
            queue.add(cacheRequest);
        }
        // DataCaching.getInstance().getRequestQueue().add(cacheRequest);
//        DataCaching.getInstance().addToRequestQueue(cacheRequest);

    }

}
