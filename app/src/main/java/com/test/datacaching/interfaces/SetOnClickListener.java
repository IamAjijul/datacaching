package com.test.datacaching.interfaces;

import com.test.datacaching.model.UserModel;

/**
 * Created by Ajijul on 11/21/2017.
 */

public interface SetOnClickListener {
    void onInfoClick(UserModel userModel);

    void onDeleteClick(int position);
}
