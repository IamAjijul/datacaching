package com.test.datacaching.helper;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.datacaching.R;
import com.test.datacaching.model.UserModel;

/**
 * Created by Khan on 11/21/2017.
 */

public class UserDetailsDialogFragment extends DialogFragment {
    private UserModel userModel;
    private Context context;
    private View view;
    private ImageView user_imvUserImage, toolbar_imvBack;
    private TextView user_tvUserName;

    public void setUserDetails(UserModel userModel, Context context) {
        this.userModel = userModel;
        this.context = context;
        Log.d("ooo","================="+userModel.getAvatar());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, R.style.FullScrrenDailog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.user_details, container, false);
        initView();
        return view;
    }

    private void initView() {
        user_imvUserImage = (ImageView) view.findViewById(R.id.user_imvUserImage);
        toolbar_imvBack = (ImageView) view.findViewById(R.id.toolbar_imvBack);
        user_tvUserName = (TextView) view.findViewById(R.id.user_tvUserName);
        Picasso.with(context)
                .load(userModel.getAvatar())
                .transform(new CircleTransform())
                .placeholder(getResources().getDrawable(R.drawable.no_image))
                .error(getResources().getDrawable(R.drawable.no_image))
                .into(user_imvUserImage);
        user_tvUserName.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        toolbar_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
