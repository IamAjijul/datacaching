package com.test.datacaching.network;

import com.android.volley.VolleyError;


public interface BaseResponseListener {
     void requestStarted();

     void requestCompleted(String response);

     void requestEndedWithError(String error);
}