package com.test.datacaching.helper;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;

/**
 * App Utility functions are here
 */
public class AppUtilities {
    public static int DEVICE_SCREEN_WIDTH = 480;
    public static int DEVICE_SCREEN_HEIGHT = 800;
    private static AppUtilities comminInstance = null;
    private Context mContext;

    private AppUtilities() {
    }

    public static AppUtilities getInstance(Context mContext) {
        if (comminInstance == null) {
            comminInstance = new AppUtilities();
        }
        comminInstance.mContext = mContext;
        return comminInstance;
    }


    public static void showSnackBar(View parentLayout, String messege, int length) {

        Snackbar snackbar = Snackbar.make(parentLayout, messege, length);
/*                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));*/

        snackbar.show();
    }


    /**
     * Check Online Status
     *
     * @return
     */
    public static boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
