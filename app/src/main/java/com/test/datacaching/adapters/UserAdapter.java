package com.test.datacaching.adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.test.datacaching.R;
import com.test.datacaching.helper.CircleTransform;
import com.test.datacaching.helper.UserDetailsDialogFragment;
import com.test.datacaching.interfaces.SetOnClickListener;
import com.test.datacaching.model.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajijul on 11/20/2017.
 *
 * this adapter use to handle the use list within recycler view
 */

public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {
    private ArrayList<UserModel> userModels = new ArrayList<>();
    private Context context;
    private SetOnClickListener listener;

    public UserAdapter(ArrayList<UserModel> userModels, Context context, SetOnClickListener setOnClickListener) {
        this.userModels = userModels;
        this.context = context;
        this.listener = setOnClickListener;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(context).inflate(R.layout.row_user, parent, false));
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        UserModel userModel = userModels.get(position);
        Picasso.with(context)
                .load(userModel.getAvatar())
                .transform(new CircleTransform())
                .placeholder(context.getResources().getDrawable(R.drawable.no_image))
                .error(context.getResources().getDrawable(R.drawable.no_image))
                .into(holder.user_imvUserImage);
        holder.user_imvInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onInfoClick(userModels.get(position));
            }
        });
        holder.user_imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDeleteClick(position);
            }
        });
    }

    private void showDetails(UserModel userModel) {

    }

    @Override
    public int getItemCount() {
        return userModels.size();
    }

    public void notifyMyAdapter(ArrayList<UserModel> userModels) {
        this.userModels = userModels;
        notifyDataSetChanged();
    }

}

class UserViewHolder extends RecyclerView.ViewHolder {
    public ImageView user_imvUserImage, user_imvInfo, user_imvDelete;

    public UserViewHolder(View itemView) {
        super(itemView);
        user_imvDelete = (ImageView) itemView.findViewById(R.id.user_imvDelete);
        user_imvInfo = (ImageView) itemView.findViewById(R.id.user_imvInfo);
        user_imvUserImage = (ImageView) itemView.findViewById(R.id.user_imvUserImage);
    }
}
