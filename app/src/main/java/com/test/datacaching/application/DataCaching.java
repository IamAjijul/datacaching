package com.test.datacaching.application;

import android.app.Application;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by Ajijul on 11/20/2017.
 */

public class DataCaching extends Application {
    private static DataCaching mInstance;
    private static RequestQueue mRequestQueue;

    public static synchronized DataCaching getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            Cache cache = new DiskBasedCache(getCacheDir(), 10 * 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
            mRequestQueue.start();
        }

        return mRequestQueue;
    }


    public void addToRequestQueue(StringRequest stringRequest) {
        getRequestQueue().add(stringRequest);

    }
}
