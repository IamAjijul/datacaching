package com.test.datacaching;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.test.datacaching.adapters.UserAdapter;
import com.test.datacaching.helper.AppUtilities;
import com.test.datacaching.helper.CircleTransform;
import com.test.datacaching.helper.MarginDecoration;
import com.test.datacaching.helper.UserDetailsDialogFragment;
import com.test.datacaching.interfaces.SetOnClickListener;
import com.test.datacaching.model.UserModel;
import com.test.datacaching.network.BaseResponseListener;
import com.test.datacaching.network.RequestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SetOnClickListener {

    private RequestClient requestClient;
    private int startIndex = 1, totalPages = 0;
    private TextView main_tvMsg;
    private RecyclerView main_rvUserList;
    private ArrayList<UserModel> userModels;
    private UserAdapter adapter;
    private boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewAndVariables();
    }

    private void initViewAndVariables() {
        requestClient = new RequestClient(this);
        main_tvMsg = (TextView) findViewById(R.id.main_tvMsg);
        main_rvUserList = (RecyclerView) findViewById(R.id.main_rvUserList);
        main_rvUserList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        main_rvUserList.addItemDecoration(new MarginDecoration(this, 8));
        userModels = new ArrayList<>();
        adapter = new UserAdapter(userModels, this, this);
        main_rvUserList.setAdapter(adapter);
        getUsersData(findViewById(R.id.rlParent), true, true);
        main_rvUserList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) //check for scroll down
                {
                    if (!loading && totalPages >= startIndex) {
                        loading = true;
                        getUsersData(findViewById(R.id.rlParent), false, false);
                    }

                }

            }
        });

    }

    private void getUsersData(View containerView, boolean showLoadingText, boolean showFullScreenLoader) {
//        if (AppUtilities.isOnline(this)) {
            requestClient.callGetUsersApi(showFullScreenLoader, this, startIndex, containerView, showLoadingText,
                    new BaseResponseListener() {
                        @Override
                        public void requestStarted() {

                        }

                        @Override
                        public void requestCompleted(String response) {
                            if (!TextUtils.isEmpty(response))
                                parseData(response);
                        }

                        @Override
                        public void requestEndedWithError(String error) {
                            main_tvMsg.setVisibility(View.VISIBLE);
                            main_rvUserList.setVisibility(View.GONE);
                            main_tvMsg.setText(error);
                        }
                    });
       /* } else {
            main_tvMsg.setVisibility(View.VISIBLE);
            main_rvUserList.setVisibility(View.GONE);
            main_tvMsg.setText(R.string.something_went_wrong);
        }
*/    }

    private void parseData(String response) {
        main_tvMsg.setVisibility(View.GONE);
        main_rvUserList.setVisibility(View.VISIBLE);
        if (!response.isEmpty() && response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                totalPages = jsonObject.getInt("total_pages");
                if (jsonArray.length() > 0) {
                    startIndex = jsonObject.getInt("page") + 1;
                    Gson gson = new Gson();
                    String jsonString = jsonObject.getJSONArray("data").toString();
                    Type typeToken = new TypeToken<List<UserModel>>() {
                    }.getType();
                    ArrayList<UserModel> users = gson.fromJson(jsonString, typeToken);
                    userModels.addAll(users);
                    Log.d("LOG", "++++" + userModels.size());
                    adapter.notifyMyAdapter(userModels);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        loading = false;
    }

    @Override
    public void onInfoClick(UserModel userModel) {
        UserDetailsDialogFragment fragment = new UserDetailsDialogFragment();
        fragment.setUserDetails(userModel, this);
        fragment.show(getSupportFragmentManager(), "DETAILS");
    }

    @Override
    public void onDeleteClick(final int position) {
        UserModel userModel = userModels.get(position);
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert, null);
        dialogBuilder.setView(dialogView);

        TextView name = (TextView) dialogView.findViewById(R.id.alert_tvName);
        name.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        ImageView imv = (ImageView) dialogView.findViewById(R.id.userImage);
        Picasso.with(this)
                .load(userModel.getAvatar())
                .transform(new CircleTransform())
                .placeholder(getResources().getDrawable(R.drawable.no_image))
                .error(getResources().getDrawable(R.drawable.no_image))
                .into(imv);
        (dialogView.findViewById(R.id.btnDelete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userModels.remove(position);
                adapter.notifyMyAdapter(userModels);
            }
        });

        final AlertDialog alertDialog = dialogBuilder.create();
        (dialogView.findViewById(R.id.btnCancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
}
